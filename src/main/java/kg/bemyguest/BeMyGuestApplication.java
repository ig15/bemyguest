package kg.bemyguest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BeMyGuestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeMyGuestApplication.class, args);
	}
}
