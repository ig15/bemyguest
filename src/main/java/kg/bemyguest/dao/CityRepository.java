package kg.bemyguest.dao;

import kg.bemyguest.domain.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Fujitsu on 23.02.2017.
 */
@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    City findCityByName(String name);
}
