package kg.bemyguest.dao;

import kg.bemyguest.domain.entities.House;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Fujitsu on 23.02.2017.
 */
@Repository
public interface HouseRepository extends PagingAndSortingRepository<House, Long> {

}
