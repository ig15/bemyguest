package kg.bemyguest.dao;

import kg.bemyguest.domain.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Fujitsu on 22.02.2017.
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findCountryByName(String name);
}
