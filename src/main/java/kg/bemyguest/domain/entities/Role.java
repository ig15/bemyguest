package kg.bemyguest.domain.entities;

/**
 * Created by Fujitsu on 22.02.2017.
 */
public enum Role {
    ROLE_ANONYMOUS, ROLE_GUEST, ROLE_HOST, ROLE_ADMIN, ROLE_USER
}

