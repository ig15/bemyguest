package kg.bemyguest.controllers;

import kg.bemyguest.domain.entities.House;
import kg.bemyguest.services.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Fujitsu on 23.02.2017.
 */
@Controller
public class PropertyController {

    @Autowired
    private HouseService houseService;

    @RequestMapping("/properties-list")
    public ModelAndView getHouses(Pageable pageable) {
        Page<House> all = houseService.getAllHouse(pageable);
        return new ModelAndView("properties-list", "houses", all);
    }


}
