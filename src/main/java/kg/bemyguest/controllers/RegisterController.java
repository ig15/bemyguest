package kg.bemyguest.controllers;

import kg.bemyguest.domain.UserDetails;
import kg.bemyguest.services.SecurityService;
import kg.bemyguest.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Fujitsu on 22.02.2017.
 */
@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    UserService service;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(method = RequestMethod.GET)
    public String register() {
        return "register";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signUp(@ModelAttribute UserDetails user, BindingResult bindingResult) {
        service.addUser(user);
        securityService.autologin(user.getEmail(), user.getPassword());
        return "redirect:/index";
    }
}
