package kg.bemyguest.services;


import kg.bemyguest.dao.CityRepository;
import kg.bemyguest.dao.CountryRepository;
import kg.bemyguest.dao.UserRepository;
import kg.bemyguest.domain.UserDetails;
import kg.bemyguest.domain.entities.City;
import kg.bemyguest.domain.entities.Country;
import kg.bemyguest.domain.entities.Role;
import kg.bemyguest.domain.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Fujitsu on 11.02.2017.
 */
@Service("userService")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CityRepository cityRepository;

    @PostConstruct
    public void init() {
        System.out.println("init method " + this.getClass().getSimpleName()+" is called");
    }

    @Transactional
    public void addUser(UserDetails userDetails) {
        User user = new User();
        user.setName(userDetails.getName());
        user.setSurname(userDetails.getSurname());
        user.setPassword(userDetails.getPassword());
        user.setEmail(userDetails.getEmail());
        user.setAddress(userDetails.getAddress());
        user.setPhoneNumber(userDetails.getPhoneNumber());
        City city = cityRepository.findCityByName(userDetails.getCity());
        Country country = countryRepository.findCountryByName(userDetails.getCountry());
        if(null != city && null != country) {
            user.setCountry_id(country.getId());
            user.setCity_id(city.getId());
        }
        user.getUserRoles().add(Role.ROLE_USER);
        userRepository.save(user);
    }

    public User login(String email, String password) {
        User user1 = userRepository.findByEmail(email);
        if(password.equals(user1.getPassword())) {
            return user1;
        }
        return null;
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }
}
