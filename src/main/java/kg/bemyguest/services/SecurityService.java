package kg.bemyguest.services;

/**
 * Created by Fujitsu on 18.02.2017.
 */
public interface SecurityService {
    String findLoggedInUsername();
    void autologin(String username, String password);
    Boolean isLoggedIn();
}
