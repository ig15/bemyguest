package kg.bemyguest.services;

import kg.bemyguest.dao.HouseRepository;
import kg.bemyguest.domain.entities.House;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by Fujitsu on 24.02.2017.
 */
@Service
public class HouseService {

    @Autowired
    private HouseRepository repository;

    public Page<House> getAllHouse(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
